//Created by Dhana Prasad
//Description : This assets allow users to search table any column value using single search without making a call to backend
import { LightningElement, api, track } from 'lwc';

export default class LWCTableSearch extends LightningElement {
    @track cappingPots = [];
    @track findedValues = [];
    @track searchValue = '';
    @track cappingPotsList = [];

    connectedCallback() {
        this.cappingPots = [{"Id":32107,"Name":"PugetPass $1.00","ValuePassCredit":100},
                            {"Id":28593,"Name":"Adult Card","ValuePassCredit":50},
                            {"Id":28597,"Name":"Youth Card","ValuePassCredit":70},
                            {"Id":28597,"Name":"Senior Card","ValuePassCredit":70},
                            {"Id":28510,"Name":"Monthly pass","ValuePassCredit":10}];
       this.cappingPotsList = this.cappingPots;
    }
 
@track count = 0;
  handleSearchCappingPots(event) {
      this.count = 1;
    this.findedValues = [];
    this.searchValue = event.target.value;
    console.log("searchValue " + this.searchValue);
    this.getHandleSearchCappingPots();
  }
  getHandleSearchCappingPots(){
    this.findedValues = [];
    for (var i = 0; i < this.cappingPots.length; i++) {
      if (
        JSON.stringify(this.cappingPots[i].Name)
          .toLowerCase()
          .match(this.searchValue.toLowerCase()) ||
        JSON.stringify(this.cappingPots[i].AmountVal)
          .toLowerCase()
          .match(this.searchValue.toLowerCase()) ||
        JSON.stringify(this.cappingPots[i].CurrentValueVal)
          .toLowerCase()
          .match(this.searchValue.toLowerCase()) ||
        JSON.stringify(this.cappingPots[i].Description)
          .toLowerCase()
          .match(this.searchValue.toLowerCase()) ||
        JSON.stringify(this.cappingPots[i].AmountToCap)
          .toLowerCase()
          .match(this.searchValue.toLowerCase())) {
        this.findedValues = [...this.findedValues, this.cappingPots[i]];
      }
    }
    this.cappingPotsList = this.findedValues;

  }
}